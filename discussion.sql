--Add new Records
--ADD 4 more artist (arstist 8-11)
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Beiber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("LBruno Mars");


--Taylor Swift(artist #5)
--For Fearless Album(album #2)
INSERT INTO songs (song_name, length, genre, album_id) VALUES("Fearless", "00:04:06", "Pop rock", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES("Love Story", "00:03:33", "Country pop", 2);

--create a new alblum for taylor swift(artist 5)
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("RED", "2012-10-22", 5);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", "00:04:33", "Rock, alternative rock, arena rock", 7);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", "00:03:24", "Country", 7);

--lady gaga
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("A Star is Born", "2018-10-05", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", "00:03:01", "Rock and roll", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", "00:03:21", "Country, rock, folk rock", 8);


INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Born This Way", "2011-05-23", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", "00:04:12", "Electropop", 9);