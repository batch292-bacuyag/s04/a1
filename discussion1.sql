mysql -u root

CREATE DATABASE record_db;

USE record_db;

CREATE TABLE artists (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE albums (
    id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    date_released DATE NOT NULL,
    artist_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_albums_artist_id
        FOREIGN KEY (artist_id) REFERENCES artists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE songs (
    id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    length TIME NOT NULL,
    genre VARCHAR(50) NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_songs_album_id
        FOREIGN KEY(album_id) REFERENCES albums(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Taylor Swift

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-11-11", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", "00:04:06", "Pop rock", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", "00:03:33", "Country pop", 1);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-10-22", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", "00:04:33", "Rock, alternative rock, arena rock", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", "00:03:24", "Country", 2);

-- Lady Gaga

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-10-05", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", "00:03:01", "Rock and roll", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", "00:03:21", "Country, rock, folk rock", 3);	

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-05-23", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", "00:04:12", "Electropop", 4);

-- Justin Bieber

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-11-13", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", "00:03:12", "Dancehall-poptropical housemoombahton", 5);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012-06-15", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", "00:04:11", "Pop", 6);

-- Ariana Grande

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016-05-20", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", "00:04:02", "EDM house", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019-02-08", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", "00:03:16", "Pop, R&B", 8);

-- Bruno Mars

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016-11-18", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", "00:03:27", "Funk, disco, R&B", 9);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011-09-11", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", "00:03:12", "Pop", 10);


--Advnace Selects

--Exclude records
--dont want to see song_id 3
SELECT * FROM songs WHERE id !=3;


--Greater than/lesser than

SELECT * FROM songs WHERE id <5;
SELECT * FROM songs WHERE id >5;

--Getting specific IDs(OR)
SELECT * FROM songs WHERE id = 2 OR id = 3 OR id = 5;
SELECT * FROM songs WHERE id = 2 OR song_name = "State of Grace";

--get specific IDS(AND)
SELECT * FROM songs WHERE id=2 AND genre="Country pop";
SELECT * FROM songs WHERE id=2 AND genre="country pop"; --will work
SELECT * FROM songs WHERE id=2 AND genre="countrypop"; --will not work

SELECT * FROM songs WHERE id=3 AND genre="kpop"; --kpop does not exist in db

--GETTING sepcific IDs(IN)
SELECT * FROM songs WHERE id IN (2,3,6,13,27); --27 will not retirn anything
SELECT * FROM songs WHERE genre IN("pop", "rock");
SELECT * FROM songs WHERE genre IN("pop", "pop rock");

--combining conditions
SELECT * FROM songs WHERE album_id = 8 and id < 13; --Thank u next
SELECT * FROM songs WHERE album_id = 2 and id < 2;
SELECT * FROM songs WHERE album_id = 3 and id < 2;
SELECT * FROM songs WHERE album_id = 3 and id < 1;
SELECT * FROM songs WHERE album_id = 1 and id < 2; --fearless


--FIND Partial Macther
--"%" and the "_"
-- & - represents all the characters that may exists before or after the given characters

SELECT * FROM songs WHERE song_name LIKE "%s"; --Fearless --black eyes
SELECT * FROM songs WHERE song_name LIKE "bo%";
SELECT * FROM songs WHERE song_name LIKE "%a%";


--_represents each character that exist before or after the given characters after the LIKE FUnction
SELECT * FROM songs WHERE song_name LIKE "____y";


--combination of % and _
SELECT * FROM songs WHERE song_name LIKE "%___t";
SELECT * FROM songs WHERE song_name LIKE "%_o_%";

--Sorting Records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

--order by and asc/desc will still perforn the 1st field defined
SELECT * FROM songs ORDER BY song_name, genre ASC; 


--distinct function - allows us to return unique fields or values
SELECT genre FROM songs;

SELECT DISTINCT genre FROM songs; -- no duplicate

--Table Joins - is a way to select data from different tables

--combine artist and albums tables
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

--combine more than t2o tables
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id;


SELECT artists.name, albums.album_title FROM artists
    JOIN albums ON artists.ID = albums.artist_id;

INSERT INTO artists(name) VALUES ("BLACKPINK");

SELECT * FROM artists
    LEFT JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists
    RIGHT JOIN albums ON artists.id = albums.artist_id;


